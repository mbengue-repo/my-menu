export const UPDATE_WITH_SUCCESS = 'Modification avec succès!';
export const ERROR_MESSAGE = 'Une erreur est survenu, contacter l\'administrateur';
export const DELETE_WITH_SUCCESS = 'Supression avec succès!';
export const DANGER = 'danger';

export const ADD_WITH_SUCCESS = 'Ajout avec succès!';
export const SUCCESS = 'success';

