import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { DANGER, ERROR_MESSAGE, SUCCESS, UPDATE_WITH_SUCCESS } from 'src/app/constant';
import { Plat } from 'src/app/models/plat';
import { PlatService } from 'src/app/services/plat.service';
import { UtilsService } from 'src/app/utils/utils.service';

@Component({
  selector: 'app-modifier',
  templateUrl: './modifier.page.html',
  styleUrls: ['./modifier.page.scss'],
})
export class ModifierPage implements OnInit {

  id: number;
  platFOrm: FormGroup;
  detailsForm: FormGroup;
  nomControl: FormControl;
  commentaireControl: FormControl;
  plat: Plat;
  constructor(
    private currentRoute: ActivatedRoute,
    private fb: FormBuilder,
    private service: PlatService,
    private utils: UtilsService
  ) {
    this.id = Number.parseInt(this.currentRoute.snapshot.paramMap.get('id'), 10);
    
    for(let plat of this.service.plats ) {
       if (plat.id ===  this.id) {
         this.plat =  plat;
          break; 
       }
    }

  }

  ngOnInit() {

    this.nomControl = new FormControl(this.plat.nom, [
      Validators.required,
      Validators.minLength(2)
    ]);

    this.platFOrm = this.fb.group({
      nom: this.nomControl,
      prix: new FormControl(this.plat.prix, [
        Validators.required,
        Validators.min(500)
      ]),
      description: new FormControl(this.plat.description)
    });

  }

  update(): void {
    let plat = this.platFOrm.value;
    plat.id = this.id;
    this.service.put(plat).subscribe(()=>{
      this.utils.presentToast(UPDATE_WITH_SUCCESS, SUCCESS, 2000);
    }, ()=>{
      this.utils.presentToast(ERROR_MESSAGE, DANGER, 2000);

    });
  }



}
