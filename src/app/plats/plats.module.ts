import { IonicModule } from '@ionic/angular';
import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { PlatsPage } from './plats.page';

import { PlatsPageRoutingModule } from './plats-routing.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    PlatsPageRoutingModule
  ],
  declarations: [PlatsPage]
})
export class PlatsPageModule {}
