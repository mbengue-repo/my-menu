import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { LoadingController, ToastController } from '@ionic/angular';
import { DANGER, DELETE_WITH_SUCCESS, ERROR_MESSAGE, SUCCESS } from '../constant';
import { Plat } from '../models/plat';
import { PlatService } from '../services/plat.service';
import { UtilsService } from '../utils/utils.service';

@Component({
  selector: 'app-plats',
  templateUrl: 'plats.page.html',
  styleUrls: ['plats.page.scss']
})
export class PlatsPage {


  plats: Plat[];
  constructor(
    private router: Router,
    private service: PlatService,
    private loadingController: LoadingController,
    private toastCtrl: ToastController,
    private utils: UtilsService
  ) {
     this.loadDatas();
  }

  
  private async loadDatas() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Chargement...',
    });
    await loading.present();

    this.service.getAll().subscribe((plats)=>{
      //cas succes
      this.service.plats = this.plats = plats;
      loading.dismiss();
   }, (error)=>{
     //echec
      console.log(error);
   });
  }

  ionViewDidEnter() {
    this.loadDatas();
  }

  redirectTo(id: number, nom: string) {
    this.router.navigate(['/tabs/plats/modifier', id, nom]);
  }

  async remove(id: number) {
    const toast = await this.toastCtrl.create({
      message: 'Suppression avec succès.',
      duration: 2000
    });

    this.service.delete(id).subscribe(()=>{
        this.loadDatas();
        toast.present();
        this.utils.presentToast(DELETE_WITH_SUCCESS, SUCCESS, 2000);
    }, ()=>{
      this.utils.presentToast(ERROR_MESSAGE, DANGER, 2000);

    })
  }
}
