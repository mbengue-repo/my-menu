import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ADD_WITH_SUCCESS, DANGER, ERROR_MESSAGE, SUCCESS } from 'src/app/constant';
import { Plat } from 'src/app/models/plat';
import { PlatService } from 'src/app/services/plat.service';
import { UtilsService } from 'src/app/utils/utils.service';

@Component({
  selector: 'app-ajouter',
  templateUrl: './ajouter.page.html',
  styleUrls: ['./ajouter.page.scss'],
})
export class AjouterPage implements OnInit {

  plat: Plat;
  constructor(
    private service: PlatService,
    private router: Router,
    private utils: UtilsService
  ) {
    this.plat = new Plat();
  }

  ngOnInit() {
  }

  performForm(plat: Plat) {
    this.service.post(plat).subscribe((plat)=>{
      this.utils.presentToast(ADD_WITH_SUCCESS, SUCCESS, 2000);  
    }, ()=>{
      this.utils.presentToast(ERROR_MESSAGE, DANGER, 2000);
    })
    this.router.navigateByUrl('/tabs/plats');
  }
}
