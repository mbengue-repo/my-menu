import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { API_URL } from 'src/environments/environment';
import { Plat } from '../models/plat';

@Injectable({
  providedIn: 'root'
})
export class PlatService {
  plats: Plat[];

  constructor(
    private httpClient: HttpClient
  ) { }

  getAll(): Observable<Plat[]> {
    return this.httpClient.get<Plat[]>(`${API_URL}/plats`);
  }

  get(id: number): Observable<Plat> {
    return this.httpClient.get<Plat>(`${API_URL}/plats/${id}`);
  }

  post(plat: Plat): Observable<Plat> {
    return this.httpClient.post<Plat>(`${API_URL}/plats/`, plat);
  }

  put(plat: Plat): Observable<Plat> {
    return this.httpClient.put<Plat>(`${API_URL}/plats/${plat.id}`, plat);

  }

  delete(id: number): Observable<Plat> {
    return this.httpClient.delete<Plat>(`${API_URL}/plats/${id}`);
  }

}
